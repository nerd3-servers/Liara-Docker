import discord
import asyncio
import inspect
import textwrap
import dis
from discord.ext import commands
from cogs.bot.utils import help as h
from cogs.bot.utils import config, p
from cogs.utils import checks

blank = "ᅟᅟᅟᅟᅟᅟᅟᅟ"
placeholder = {
    "embed_author":
    "❓ Bot Help",
    "embed_logo":
    None,
    "embed_desc":
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin egestas non nibh non cursus. Curabitur eget libero lectus. Vivamus tincidunt interdum dolor et tempus.",
    "fields": [{
        "name": "Made possible using",
        "value":
        "[discord.py](https://github.com/Rapptz/discord.py/tree/rewrite)\n[Liara Docker](https://gitlab.com/nerd3-servers/liara-docker)",
        "inline": False
    },
               {
                   "name": blank,
                   "value":
                   "[Liara by Pandentia](https://github.com/Thessia/Liara)",
                   "inline": False
               }]
}


class help(commands.Cog):
    """Custom help messages WITH FANCY EMBEDS OOOOOO!"""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'General',
            'image': 'https://i.imgur.com/AZWeMcH.png'
        }
        self.config = config.Load('help').dict
        self.liara.send_command_help = h.sendHelp

    def get_help_embed(self):
        embed = discord.Embed()
        description = self.config.get('embed_desc', None)
        if not description:
            description = placeholder['embed_desc']
        embed.description = description
        author = self.config.get('embed_author', None)
        if not author:
            author = placeholder['embed_desc']
        embed.set_author(name=author)
        logo = self.config.get('embed_logo', None)
        if not logo:
            logo = self.liara.user.avatar_url_as(static_format='png')
        embed.set_thumbnail(url=logo)
        fields = self.config.get('fields', None)
        if fields is None:
            fields = placeholder['fields']
        for field in fields:
            name = field.get('name', blank)
            if not name:
                name = blank
            value = field.get('value', blank)
            if not value:
                value = blank
            inline = field.get('inline', True)
            embed.add_field(name=name, value=value, inline=inline)
        return embed

    def field_formatter(self, string: str):
        """Formats an embed field so it's smol."""
        return textwrap.fill(string, 25)

    async def help_groups(self):
        """Returns all different help groups and their commands."""
        groups = {'General': []}
        core = []
        for command in self.liara.commands:
            cog = command.cog_name
            if cog == 'Core':
                core.append(command)
            elif cog:
                cog = self.liara.get_cog(cog)
                try:
                    settings = cog.help_set
                    if settings.get('group'):
                        if settings['group'] == 'Core':
                            core.append(command)
                        else:
                            if not groups.get(settings['group']):
                                groups[settings['group']] = [command]
                            else:
                                groups[settings['group']].append(command)
                    else:
                        groups['General'].append(command)
                except AttributeError:
                    groups['General'].append(command)
            else:
                groups['General'].append(command)
        if core:
            groups['Core'] = core
        return groups

    async def can_run(self, ctx, command):
        for function in command.checks:
            try:
                if inspect.iscoroutinefunction(function):
                    if not await function(ctx):
                        return False
                else:
                    if not function(ctx):
                        return False
            except commands.NoPrivateMessage:
                pass
            except p.InvalidChannel:
                pass
            except:
                return False
        return True

    async def help_formatter(self, ctx):
        groups = await self.help_groups()
        embeds = []
        prefix = self.liara.command_prefix[0]
        # Build the top embed
        emb = self.get_help_embed()
        embeds.append(emb)
        fields = 0

        for group in groups:
            cmd_group = sorted(groups[group], key=lambda x: x.name)
            emb = discord.Embed()
            emb.set_author(name=f'❓ {group} Commands')
            fields = 0
            for command in cmd_group:
                hidden = False
                if command.hidden:
                    hidden = True
                    if not checks.owner_check(ctx):
                        # Not a bot owner, so don't show them the hidden command
                        continue
                if not await self.can_run(ctx, command):
                    continue

                if fields == 24:
                    embeds.append(emb)
                    emb = discord.Embed()
                    emb.set_author(name=f'❓ {group} Commands (Cont.)')
                    fields = 0
                info = self.field_formatter(
                    command.short_doc) if command.short_doc else blank
                emb.add_field(
                    name=(f'`{prefix}{command.name}`{"🕵" if hidden else ""}'),
                    value=info)
                fields += 1

            if (emb not in embeds and emb.fields):
                if fields % 3 == 2:
                    emb.add_field(name=blank, value=blank)

                embeds.append(emb)
        embeds[-1].set_footer(
            text=f'Do {prefix}help <command> for more info on a command.')

        # Let's give the embeds some colour
        if not isinstance(ctx.channel, discord.abc.PrivateChannel):
            for emb in embeds:
                emb.colour = ctx.guild.me.colour

        # Returns all the embeds
        return embeds

    @commands.command()
    async def help(self, ctx, *, command=None):
        """Returns a list of all commands you can run, or help on a specific command.
        
        Example
        -------
        `[p]help help`"""
        if not command:
            try:
                await ctx.message.delete()
            except:
                pass
            embeds = await self.help_formatter(ctx)
            for emb in embeds:
                try:
                    await ctx.author.send(embed=emb)
                except:
                    await ctx.send(
                        f'{ctx.author.mention} You don\'t have your DMs open!',
                        delete_after=5)
                    return
        else:
            command = self.liara.get_command(command)
            if isinstance(ctx.channel, discord.abc.PrivateChannel):
                if await self.can_run(ctx, command):
                    emb = await h.commandEmbed(ctx, command)
                    await ctx.send(embed=emb)
            else:
                if (command and await command.can_run(ctx)):
                    emb = await h.commandEmbed(ctx, command)
                    colour = ctx.guild.me.colour
                    emb.colour = colour
                    await ctx.send(embed=emb)


def setup(liara):
    liara.remove_command('help')
    liara.add_cog(help(liara))
