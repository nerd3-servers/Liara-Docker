import json
import os


class Load:

    def __init__(self, file):
        self.file = file
        config = os.environ['CONFIG']
        with open(f'{config}/{self.file}.json', 'r') as f:
            self.dict = json.load(f)

    def get(self, key, return_none: bool = False):
        try:
            return self.dict[key]
        except:
            if return_none:
                return None
            return key
