import asyncio
import discord
from cogs.utils import checks
from discord.ext import commands
from discord.ext.commands import errors
from cogs.utils.storage import RedisCollection
from cogs.bot import permissions

error = 'Command {} can\'t be run in {}.'


class InvalidChannelType(errors.CheckFailure):
    pass


class InvalidChannelId(errors.CheckFailure):
    pass


async def is_staff(ctx):
    if checks.owner_check(ctx):
        return True
    if not ctx.guild:
        return False
    if ctx.author == ctx.guild.owner:
        return True
    try:
        if await checks.role_check(ctx, 'mod'):
            return True
    except:
        pass
    try:
        if await checks.role_check(ctx, 'admin'):
            return True
    except:
        pass
    return False


async def channel_types(liara, channel):
    """Returns what channel type(s) a channel belongs to as a list."""
    db = RedisCollection(liara.redis, 'permissions')
    channel_types = await db.get('channel_types', {})
    types = []
    for channel_type in channel_types:
        servers = channel_types[channel_type]
        if not servers:
            continue
        if not servers.get(channel.guild.id):
            continue
        if channel.id not in servers[channel.guild.id]:
            continue
        types.append(channel_type)
    return types


def staff_or_channel_type(*types):

    async def predicate(ctx: commands.Context = None):
        if not ctx:
            return types
        db = RedisCollection(ctx.bot.redis, 'permissions')
        channel_types = await db.get('channel_types')

        if await is_staff(ctx):
            return True

        e = error.format(ctx.command.name, str(ctx.channel.id))

        for channeltype in types:
            servers = channel_types[channeltype]
            if not servers:
                raise InvalidChannelType(e, types)
            if not servers.get(ctx.guild.id):
                raise InvalidChannelType(e, types)
            if ctx.channel.id in servers[ctx.guild.id]:
                return True
        raise InvalidChannelType(e, types)

    return commands.check(predicate)


def staff_or_channel_id(*ids):

    async def predicate(ctx):
        if await is_staff(ctx):
            return True
        if ctx.channel.id in ids:
            return True
        raise InvalidChannelId(
            error.format(ctx.command.name, str(ctx.channel.id)), ids)

    return commands.check(predicate)


async def def_channel_type(liara, *types):
    # Used for defining new channel types.
    print(types)
    db = RedisCollection(liara.redis, 'permissions')
    channel_types = await db.get('channel_types', {})
    for t in types:
        if t not in channel_types:
            print(f'Defining new type {t} from {types}')
            channel_types[t] = {}
            await db.set('channel_types', channel_types)
