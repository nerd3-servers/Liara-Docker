import discord
import asyncio
from discord.ext import commands
from cogs.utils.storage import RedisCollection
from cogs.utils import checks
from cogs.bot.utils import help, p


class permissions(commands.Cog):
    """Channel based permissions."""

    def __init__(self, liara):
        self.liara = liara
        self.db = RedisCollection(liara.redis, 'permissions')
        self.help_set = {
            'group': 'Mod',
            'image': 'https://i.imgur.com/5Jd67Cp.png'
        }

    @commands.group('p', aliases=['permissions'], invoke_without_command=True)
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def p(self, ctx):
        """Manage channel based permissions."""
        await help.sendHelp(ctx)

    @p.command('list')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def plist(self, ctx):
        """List the different command groups and the channels that are assigned to them."""
        channel_types = await self.db.get('channel_types', {})
        if not channel_types:
            return await ctx.send(
                'No command groups have been defined. Please see the documentation for more info.'
            )
        text = ''
        for channeltype in channel_types:
            text += '**%s**\n' % (channeltype)
            channeltype = channel_types[channeltype]
            if not channeltype:
                text += 'No channels have been assigned to this type.\n\n'
            elif not channeltype.get(ctx.guild.id, False):
                text += 'No channels have been assigned to this type.\n\n'
            else:
                channellist = []
                for channel_id in channeltype[ctx.guild.id]:
                    channel = self.liara.get_channel(channel_id)
                    channellist.append(channel.mention)
                text += '%s\n\n' % (', '.join(str(x) for x in channellist))
        await ctx.send(text)

    @p.command('add')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def padd(self, ctx, group: str, channel: discord.TextChannel):
        """Add a channel to a command group.
        
        Do !p list to list all command groups.
        
        Example
        -------
        `[p]p add shitposting #meme-central`"""
        channel_types = await self.db.get('channel_types', {})
        if not channel_types:
            return await ctx.send(
                'No command groups have been defined. Please see the documentation for more info.'
            )
        if group not in channel_types:
            return await ctx.send(
                'This command group does not exist. Please see the documentation for more info.'
            )
        if ctx.guild.id not in channel_types[group]:
            channel_types[group][ctx.guild.id] = [channel.id]
            await self.db.set('channel_types', channel_types)
            await ctx.send('Channel %s added to command group %s.' %
                           (channel.mention, group))
        else:
            if channel.id in channel_types[group][ctx.guild.id]:
                return await ctx.send('Channel %s is already in group %s.' %
                                      (channel.mention, group))
            channels = channel_types[group][ctx.guild.id]
            channels.append(channel.id)
            channel_types[group][ctx.guild.id] = channels
            await self.db.set('channel_types', channel_types)
            await ctx.send('Channel %s added to command group %s.' %
                           (channel.mention, group))

    @p.command('remove')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def premove(self, ctx, group: str, channel: discord.TextChannel):
        """Remove a channel from a command group.
        
        Do !p list to list all command groups.
        
        Example
        -------
        `[p]p remove shitposting #meme-central`"""
        channel_types = await self.db.get('channel_types', {})
        if not channel_types:
            return await ctx.send(
                'No command groups have been defined. Please see the documentation for more info.'
            )
        if group not in channel_types:
            return await ctx.send(
                'That command group does not exist. Please see the documentation for more info.'
            )
        if ctx.guild.id not in channel_types[group]:
            return await ctx.send(
                'Channel %s is not assigned to command group %s.' %
                (channel.mention, group))
        if channel.id not in channel_types[group][ctx.guild.id]:
            return await ctx.send(
                'Channel %s is not assigned to command group %s.' %
                (channel.mention, group))
        else:
            channels = channel_types[group][ctx.guild.id]
            channels.remove(channel.id)
            channel_types[group][ctx.guild.id] = channels
            await self.db.set('channel_types', channel_types)
            await ctx.send('Channel %s removed from command group %s.' %
                           (channel.mention, group))

    async def update_groups(self):
        for command in self.liara.commands:
            for function in command.checks:
                if function.__qualname__.startswith(
                        'staff_or_channel_type'
                ) and function.__module__ == 'cogs.bot.utils.p':
                    types = await function()
                    if types:
                        await p.def_channel_type(self.liara, *types)

    @commands.Cog.listener()
    async def on_cog_load(self, cog):
        await self.update_groups()


def setup(liara):
    cog = permissions(liara)
    liara.loop.create_task(cog.update_groups())
    liara.add_cog(cog)
